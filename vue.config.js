module.exports = {
    productionSourceMap: false,
    devServer: {
        port: 3333
    },
    publicPath: '/'
}